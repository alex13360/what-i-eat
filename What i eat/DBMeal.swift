//
//  DBMeal.swift
//  What i eat
//
//  Created by Alex on 2/9/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DBMeal: NSManagedObject {
    
    convenience init(name: String, date: Date, calories: Double) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
      
        let managedContext = appDelegate.managedObjectContext
        
        let entity = NSEntityDescription.entity(forEntityName: "DBMeal", in: managedContext)
        
        self.init(entity: entity!, insertInto: managedContext)

        self.name = name
        self.date = date
        self.calories = calories
        
        managedContext.delete(self)
    }
    
    func formattedNameString() -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: name as String)
        
        let pararaphStyle = NSMutableParagraphStyle()
        pararaphStyle.lineSpacing = 15
        
        let attributes = [NSFontAttributeName:UIFont().helveticaNeueLTStdWithSize(18),
            NSForegroundColorAttributeName: AppColor.lightGreyColor(),
            NSParagraphStyleAttributeName: pararaphStyle]
        
        attributedString.addAttributes(attributes, range: NSMakeRange(0, name.characters.count))
        
        return attributedString
    }
    
    func formattedCaloriesString() -> NSMutableAttributedString {
        if calories == 0 {
            return NSMutableAttributedString()
        }
        
        let sufix = "kcal"
       
        var convertedCalories: Any = calories
        
        if (calories - floor(calories)) > 0.0001 {
            convertedCalories = calories
        } else {
            convertedCalories = Int(calories)
        }
        
        let caloriesString = String(describing: convertedCalories) + " " +  sufix as NSString
        let attributedString = NSMutableAttributedString(string: caloriesString as String)
        
        let pararaphStyle = NSMutableParagraphStyle()
        pararaphStyle.alignment = NSTextAlignment.right
        
        let firstAttributes = [NSParagraphStyleAttributeName: pararaphStyle,
            NSFontAttributeName:UIFont().helveticaNeueLTStdWithSize(15),
            NSForegroundColorAttributeName: AppColor.lightGreyColor()]
        let secondAttributes = [NSFontAttributeName:UIFont().helveticaNeueLTStdWithSize(9),
            NSForegroundColorAttributeName: AppColor.darkGreyColor()]
        
        attributedString.addAttributes(firstAttributes, range: caloriesString.range(of: String(describing: convertedCalories)))
        attributedString.addAttributes(secondAttributes, range: caloriesString.range(of: sufix))
        
        return attributedString
    }
    
    func formattedTimeString() -> String {
        return date.formattedHoursMinutesLocale.uppercased()
    }
}
