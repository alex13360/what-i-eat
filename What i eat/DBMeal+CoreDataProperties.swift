//
//  DBMeal+CoreDataProperties.swift
//  What i eat
//
//  Created by Alex on 2/9/16.
//  Copyright © 2016 Alex. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DBMeal {
    
    @NSManaged var calories: Double
    @NSManaged var date: Date
    @NSManaged var name: String
    
}
