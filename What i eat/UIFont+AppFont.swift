//
//  UIFontExtensions.swift
//  What i eat
//
//  Created by Alex on 1/25/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    func helveticaNeueLTStdWithSize(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueLTStd-Lt", size: size)!
    }
}
