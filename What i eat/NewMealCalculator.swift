//
//  NewMealCalculator.swift
//  What i eat
//
//  Created by Alex on 2/1/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

protocol NewMealCalculatorDelegate: class {
    func newMealCalculatorDidCalculate(_ control: NewMealCalculator, didPressedButtonAtIndex index: Int)
}

@IBDesignable class NewMealCalculator: UIView {

//MARK: Internal Properties
    
    weak var delegate: NewMealCalculatorDelegate? = nil
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var zeroButton: UIButton!
    @IBOutlet fileprivate weak var oneButton: UIButton!
    @IBOutlet fileprivate weak var twoButton: UIButton!
    @IBOutlet fileprivate weak var threeButton: UIButton!
    @IBOutlet fileprivate weak var fourButton: UIButton!
    @IBOutlet fileprivate weak var fiveButton: UIButton!
    @IBOutlet fileprivate weak var sixButton: UIButton!
    @IBOutlet fileprivate weak var sevenButton: UIButton!
    @IBOutlet fileprivate weak var eightButton: UIButton!
    @IBOutlet fileprivate weak var nineButton: UIButton!
    @IBOutlet fileprivate weak var dotButton: UIButton!
    @IBOutlet fileprivate weak var cleanButton: UIButton!
    @IBOutlet fileprivate weak var plusButton: UIButton!
    @IBOutlet fileprivate weak var multiplyButton: UIButton!
    @IBOutlet fileprivate weak var equalButton: UIButton!
    @IBOutlet fileprivate weak var eraseButton: UIButton!
    
    fileprivate var view: UIView!
    
//MARK: Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
}
    
//MARK: Private API

private extension NewMealCalculator {
    @IBAction func calculate(_ sender: UIButton) {
        delegate?.newMealCalculatorDidCalculate(self, didPressedButtonAtIndex: sender.tag)
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
      
        let nib = UINib(nibName: "NewMealCalculator", bundle: bundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
