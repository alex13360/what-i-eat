//
//  NewMealSegmentedControl.swift
//  What i eat
//
//  Created by Alex on 1/26/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

protocol NewMealSegmentedControlDelegate: class {
    func newMealSegmentedControl(_ control: NewMealSegmentedControl, didPressedButtonAtIndex index: Int, previousStateIndex previousIndex: Int)
}

@IBDesignable class NewMealSegmentedControl: UIView {
    
//MARK: Internal Properties
    
    weak var delegate: NewMealViewController? = nil
    
    var lockButtons = false {
        didSet {
            addMealButton.isEnabled = !lockButtons
            addCaloriesButton.isEnabled = !lockButtons
            addTimeButton.isEnabled = !lockButtons
        }
    }
    var timeString = "" {
        didSet {
            addTimeTextLabel.text = timeString
        }
    }
    var calories = 0.0 {
        didSet {
            if calories == 0 {
                addCaloriesTextLabel.text = "Calories"
            }
            else {
                if (calories - floor(calories)) > 0.001 {
                    addCaloriesTextLabel.text = String(calories) + " Kcal"
                } else {
                    addCaloriesTextLabel.text = String(Int(calories)) + " Kcal"
                }
            }
        }
    }
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var addMealBackgroundView: UIView!
    @IBOutlet fileprivate weak var addMealTextLabel: UILabel!
    @IBOutlet fileprivate weak var addMealImageView: UIImageView!
    @IBOutlet fileprivate weak var addMealButton: UIButton!
    
    @IBOutlet fileprivate weak var addCaloriesBackgroundView: UIView!
    @IBOutlet fileprivate weak var addCaloriesTextLabel: UILabel!
    @IBOutlet fileprivate weak var addCaloriesImageView: UIImageView!
    @IBOutlet fileprivate weak var addCaloriesButton: UIButton!

    @IBOutlet fileprivate weak var addTimeBackgroundView: UIView!
    @IBOutlet fileprivate weak var addTimeTextLabel: UILabel!
    @IBOutlet fileprivate weak var addTimeImageView: UIImageView!
    @IBOutlet fileprivate weak var addTimeButton: UIButton!
    
    fileprivate var view: UIView!
    
    fileprivate var previousState = NewMealSegmentedControlItem.title.rawValue
    
    fileprivate enum NewMealSegmentedControlItem: Int {
        case title
        case calories
        case time
    }

//MARK: Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
}

//MARK: Private

private extension NewMealSegmentedControl {
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
       
        let nib = UINib(nibName: "NewMealSegmentedControl", bundle: bundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @IBAction func addMeal(_ sender: UIButton) {
        addMealBackgroundView.backgroundColor = AppColor.greenColor()
        addCaloriesBackgroundView.backgroundColor = AppColor.greyColor()
        addTimeBackgroundView.backgroundColor = AppColor.greyColor()
        
        addMealTextLabel.textColor = AppColor.greyColor()
        addCaloriesTextLabel.textColor = AppColor.whiteColor()
        addTimeTextLabel.textColor = AppColor.whiteColor()
        
        addMealImageView.image = UIImage(named: "add_meal_pressed")
        addCaloriesImageView.image = UIImage(named: "add_calories")
        addTimeImageView.image = UIImage(named: "add_time")
        
        delegate?.newMealSegmentedControl(self, didPressedButtonAtIndex: NewMealSegmentedControlItem.title.rawValue, previousStateIndex: previousState)
       
        previousState = NewMealSegmentedControlItem.title.rawValue
    }
    
    @IBAction func addCalories(_ sender: UIButton) {
        addMealBackgroundView.backgroundColor = AppColor.greyColor()
        addCaloriesBackgroundView.backgroundColor = AppColor.greenColor()
        addTimeBackgroundView.backgroundColor = AppColor.greyColor()
        
        addMealTextLabel.textColor = AppColor.whiteColor()
        addCaloriesTextLabel.textColor = AppColor.greyColor()
        addTimeTextLabel.textColor = AppColor.whiteColor()
        
        addMealImageView.image = UIImage(named: "add_meal")
        addCaloriesImageView.image = UIImage(named: "add_calories_pressed")
        addTimeImageView.image = UIImage(named: "add_time")
        
        delegate?.newMealSegmentedControl(self, didPressedButtonAtIndex: NewMealSegmentedControlItem.calories.rawValue, previousStateIndex: previousState)
       
        previousState = NewMealSegmentedControlItem.calories.rawValue
    }
    
    @IBAction func addTime(_ sender: UIButton) {
        addMealBackgroundView.backgroundColor = AppColor.greyColor()
        addCaloriesBackgroundView.backgroundColor = AppColor.greyColor()
        addTimeBackgroundView.backgroundColor = AppColor.greenColor()
        
        addMealTextLabel.textColor = AppColor.whiteColor()
        addCaloriesTextLabel.textColor = AppColor.whiteColor()
        addTimeTextLabel.textColor = AppColor.greyColor()
        
        addMealImageView.image = UIImage(named: "add_meal")
        addCaloriesImageView.image = UIImage(named: "add_calories")
        addTimeImageView.image = UIImage(named: "add_time_pressed")
        
        delegate?.newMealSegmentedControl(self, didPressedButtonAtIndex: NewMealSegmentedControlItem.time.rawValue, previousStateIndex: previousState)
       
        previousState = NewMealSegmentedControlItem.time.rawValue
    }
}
