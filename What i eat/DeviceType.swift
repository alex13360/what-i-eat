//
//  DeviceType.swift
//  What i eat
//
//  Created by Alex on 1/29/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize {
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let screenMaxLength = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

struct DeviceType {
    static let isIphone4OrLess =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength < 568.0
    static let isIphone5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 568.0
    static let isIphone6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 667.0
    static let isIphone6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 736.0
}
