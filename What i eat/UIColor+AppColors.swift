//
//  UIColorExtensions.swift
//  What i eat
//
//  Created by Alex on 1/25/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: String) {
        let redRange    = (hex.characters.index(hex.startIndex, offsetBy: 1) ..< hex.characters.index(hex.startIndex, offsetBy: 3))
        let greenRange  = (hex.characters.index(hex.startIndex, offsetBy: 3) ..< hex.characters.index(hex.startIndex, offsetBy: 5))
        let blueRange   = (hex.characters.index(hex.startIndex, offsetBy: 5) ..< hex.characters.index(hex.startIndex, offsetBy: 7))
        
        var red     : UInt32 = 0
        var green   : UInt32 = 0
        var blue    : UInt32 = 0
        
        Scanner(string: hex.substring(with: redRange)).scanHexInt32(&red)
        Scanner(string: hex.substring(with: greenRange)).scanHexInt32(&green)
        Scanner(string: hex.substring(with: blueRange)).scanHexInt32(&blue)
        
        self.init(
            red: CGFloat(red) / 255,
            green: CGFloat(green) / 255,
            blue: CGFloat(blue) / 255,
            alpha: 1
        )
    }
}
