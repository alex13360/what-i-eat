//
//  Constants.swift
//  What i eat
//
//  Created by Alex on 2/1/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation

struct UserDefaults {
    static let caloriesEnabled = "caloriesEnabled"
    static let fxSoundEnabled = "fxSoundEnabled"
}

struct Documents {
    static let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
}

enum NewMealSegment: Int {
    case name
    case calories
    case time
}

enum TimeStyle {
    case format12
    case format24
}

let maxClaculatedValue = 99999.0

