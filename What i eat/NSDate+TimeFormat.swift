//
//  NSDate+TimeFormat.swift
//  What i eat
//
//  Created by Alex on 2/2/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation

extension Date {
    var formattedHoursMinutesLocale: String {
        let formatter = DateFormatter()
        
        if self.dateFormat == TimeStyle.format12 {
            formatter.dateFormat = "hh:mm a"
        } else {
            formatter.dateFormat = "HH:mm"
        }

        return  formatter.string(from: self)
    }
    var formattedHours: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
       
        return  formatter.string(from: self)
    }
    var formattedMinutes: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm"
      
        return  formatter.string(from: self)
    }
    var dateFormat: TimeStyle {
        let locale = Locale.current
        let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: locale)!
       
        if dateFormat.range(of: "a") != nil {
           return TimeStyle.format12
        }
        else {
            return TimeStyle.format24
        }
    }
}
