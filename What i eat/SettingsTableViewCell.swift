//
//  SettingsTableViewCell.swift
//  What i eat
//
//  Created by Alex on 2/1/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
//MARK: Internal Properties
    
    @IBOutlet weak var stateSwitch: UISwitch!
    
    var propertyString = "" {
        didSet {
            propertyLabel.text = propertyString
        }
    }
    var propertyState = true {
        didSet {
            stateSwitch.setOn(propertyState, animated: false)
        }
    }
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var propertyLabel: UILabel!
    
//MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
