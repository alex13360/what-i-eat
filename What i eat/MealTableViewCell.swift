//
//  MealTableViewCell.swift
//  What i eat
//
//  Created by Alex on 1/18/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    
//MARK: Internal Properties
    
    var mealName: NSMutableAttributedString? {
        didSet {
            mealListTextView.attributedText = mealName
        }
    }
    var caloriesValue: NSMutableAttributedString? {
        didSet {
            caloriesLabel.attributedText = caloriesValue
        }
    }
    var timeValue = "" {
        didSet {
            timeLabel.text = timeValue
        }
    }
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var mealListTextView: UITextView!
    @IBOutlet fileprivate weak var caloriesLabel: UILabel!
    
//MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
