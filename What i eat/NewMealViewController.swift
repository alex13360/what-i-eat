//
//  NewMealViewController.swift
//  What i eat
//
//  Created by Alex on 1/21/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import CoreData
import Foundation

protocol NewMealViewControllerDelegate: class {
    func newMealViewController(_ controller: NewMealViewController, didAddMeal meal: DBMeal)
    func newMealViewController(_ controller: NewMealViewController, didUpdateMeal meal: DBMeal)
}

class NewMealViewController: UIViewController {
 
//MARK: Internal Properties
    
    weak var delegate: NewMealViewControllerDelegate? = nil
    var mealToEdit: DBMeal? = nil
    var currentDay = Date()

//MARK: Private Properties
    
    @IBOutlet fileprivate weak var cancelButton: UIButton!
    @IBOutlet fileprivate weak var addNewMealButton: UIButton!
    @IBOutlet fileprivate weak var newFoodLabel: UILabel!
    @IBOutlet fileprivate weak var shelfImageView: UIImageView!
    @IBOutlet fileprivate weak var segmentedControlView: NewMealSegmentedControl!
    
    @IBOutlet fileprivate weak var addMealView: UIView!
    @IBOutlet fileprivate weak var addMealTextView: UITextView!
    @IBOutlet fileprivate weak var addMealKeyboardImageView: UIImageView!
    
    @IBOutlet fileprivate weak var addCaloriesView: UIView!
    @IBOutlet fileprivate weak var hintImageView: UIImageView!
    @IBOutlet fileprivate weak var panToChangeLabel: UILabel!
    @IBOutlet fileprivate weak var caloriesLabel: UILabel!
    @IBOutlet fileprivate weak var calculateCaloriesView: UIView!
    
    @IBOutlet fileprivate weak var calculatorView: NewMealCalculator!
    
    @IBOutlet fileprivate weak var addTimeView: UIView!
    @IBOutlet fileprivate weak var datePicker: UIDatePicker!
    @IBOutlet fileprivate weak var hintTimeImageVIew: UIImageView!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    
    @IBOutlet fileprivate var calculateCaloriesGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet fileprivate var changeCaloriesValueRecognizer: UIPanGestureRecognizer!
    @IBOutlet fileprivate var changeTimeValueRecognizer: UIPanGestureRecognizer!
    
    @IBOutlet fileprivate weak var keyboardHightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var addNewMealLeadingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var addCaloriesLeadingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var addTimeLeadingConstraint: NSLayoutConstraint!
    
    fileprivate var calculatedString = "0"
    fileprivate var calculatedCalories = 0.0
    fileprivate var isEdit: Bool {
        return mealToEdit != nil
    }

//MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        setDefaultValueForViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewMealViewController.keyboardDidChangeFrame(_:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func keyboardDidChangeFrame(_ sender: Notification) {
        if let userInfo = sender.userInfo {
            let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
                if (keyboardHightConstraint.constant != keyboardHeight) {
                    keyboardHightConstraint.constant = keyboardHeight
                   
                    self.view.layoutIfNeeded()
                }
        }
    }
    
    func updateTime() {
        timeLabel.text = datePicker.date.formattedHoursMinutesLocale
        currentDay = datePicker.date
    }
}

// MARK: - UITextViewDeleagate

extension NewMealViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let joiner = "\n"
        let marker = "•"
        
        if text == joiner {
            // If the replacement text is being added to the end
            if range.location == textView.text.characters.count{
                let bulletListArray = textView.text.components(separatedBy: joiner)
                if bulletListArray.count == 1 && !bulletListArray[0].hasPrefix(marker) {
                    textView.text = marker + " " + textView.text
                }
                var updatedText = textView.text + joiner + marker + " "
                updatedText.replaceSubrange(updatedText.startIndex...updatedText.startIndex, with: String(updatedText[updatedText.startIndex]).capitalized)
                textView.text = updatedText
            }
            // Else if the replacement text is being added in the middle
            else {
                let beginning = textView.beginningOfDocument;
                let start = textView.position(from: beginning, offset: range.location)
                let end = textView.position(from: start!, offset: range.length)
                let textRange = textView.textRange(from: start!, to: end!)
                
                // Insert that newline character
                textView.replace(textRange!, withText:joiner+marker + " ")
                // Update the cursor position accordingly
                let cursor = NSMakeRange(range.location + (joiner + marker).characters.count, 0)
                textView.selectedRange = cursor
            }
            return false
        }
        return true
    }
}

// MARK: - SegmentedControlDelegate

extension NewMealViewController: NewMealSegmentedControlDelegate {
    func newMealSegmentedControl(_ control: NewMealSegmentedControl, didPressedButtonAtIndex index: Int, previousStateIndex previousIndex: Int) {
        guard previousIndex != index else {
            return
        }
        
        switch previousIndex {
        case NewMealSegment.calories.rawValue:
            calculateString()
            if calculatedCalories == 0 {
                segmentedControlView.calories = 0
            } else {
                if calculatedCalories > maxClaculatedValue {
                    calculatedCalories = 99999
                    calculatedString = caloriesNumberToString(calculatedCalories)
                    caloriesLabel.text = caloriesNumberToString(calculatedCalories)
                    segmentedControlView.calories = calculatedCalories
                } else {
                    segmentedControlView.calories = calculatedCalories
                }
            }
        case NewMealSegment.time.rawValue:
            segmentedControlView.timeString = currentDay.formattedHoursMinutesLocale
        default: break
        }

        segmentedControlView.lockButtons = true

        if index == NewMealSegment.calories.rawValue && previousIndex == NewMealSegment.name.rawValue {
            goFromMealToCalories()
        }
        
        if index == NewMealSegment.name.rawValue && previousIndex == NewMealSegment.calories.rawValue {
            goFromCaloriesToMeal()
        }
        
        if index == NewMealSegment.time.rawValue && previousIndex == NewMealSegment.calories.rawValue {
            goFromCaloriesToTime()
        }
        
        if index == NewMealSegment.calories.rawValue && previousIndex == NewMealSegment.time.rawValue {
            goFromTimeToCalories()
        }
        
        if index == NewMealSegment.time.rawValue && previousIndex == NewMealSegment.name.rawValue {
            goFromMealToTime()
        }
        
        if index == NewMealSegment.name.rawValue && previousIndex == NewMealSegment.time.rawValue {
           goFromTimeToMeal()
        }
        
        switch index {
        case NewMealSegment.name.rawValue:
            newFoodLabel.text = "New Food"
        case NewMealSegment.calories.rawValue:
            hideAddNewMealTextViewKeyboard()
           
            newFoodLabel.text = "Set Calories"
        case NewMealSegment.time.rawValue:
            hideAddNewMealTextViewKeyboard()

            newFoodLabel.text = "Set Time"
        default: break
        }
    }
}

// MARK: - NewMealCalculatorDelegate

extension NewMealViewController: NewMealCalculatorDelegate {
    func newMealCalculatorDidCalculate(_ control: NewMealCalculator, didPressedButtonAtIndex index: Int) {
        calculatorButtonPressed(index)
    }
}

//MARK: Private

private extension NewMealViewController {
    struct Time {
        var minutes: Int
        var hours: Int
        
        init() {
            minutes = 0
            hours = 0
        }
    }
  
    @IBAction func addNewMeal(_ sender: UIButton) {
        let processedMealName = deleteEmptyRowsFromMealTextView(addMealTextView)
        
        calculateString()
        
        if processedMealName.characters.count > 0 {
            if calculatedCalories > maxClaculatedValue {
                calculatedCalories = 99999
                calculatedString = caloriesNumberToString(calculatedCalories)
                caloriesLabel.text = caloriesNumberToString(calculatedCalories)
                segmentedControlView.calories = calculatedCalories
            }
            
            let newMeal = DBMeal(name: processedMealName, date: datePicker.date , calories: calculatedCalories)
            
            if isEdit {
                delegate?.newMealViewController(self, didUpdateMeal: newMeal)
            } else {
                delegate?.newMealViewController(self, didAddMeal: newMeal)
            }
        }
        
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func calculateString(_ sender: UITapGestureRecognizer) {
        calculateString()
    }
    
    @IBAction func changeCalories(_ sender: UIPanGestureRecognizer) {
        if  sender.state == UIGestureRecognizerState.ended {
            return
        }
        
        let direction = sender.velocity(in: calculateCaloriesView)
        
        calculateString()
        
        if direction.x > 0 {
            calculatedCalories += 1
        } else if direction.x < 0 {
            calculatedCalories = calculatedCalories <= 1 ? 0 : calculatedCalories - 1
        } else {
            return
        }
        
        calculatedString = caloriesNumberToString(calculatedCalories)
        caloriesLabel.text = calculatedString
    }
    
    @IBAction func changeTime(_ sender: UIPanGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.ended {
            datePicker.setDate(currentDay, animated: true)
            
            return
        }
        
        let direction = sender.velocity(in: addTimeView)
        
        var time: Time = Time()
        time.minutes = Int(currentDay.formattedMinutes)!
        time.hours = Int(currentDay.formattedHours)!
        
        if direction.x > 0 {
            time.minutes += 1
            
            if time.minutes > 59 {
                time.minutes = 0
                
                time.hours += 1
                
                if time.hours > 23 {
                    time.hours = 0
                }
            }
            
            currentDay = changeTime(time, toDate: currentDay)
        }
        else if direction.x < 0 {
            time.minutes -= 1
            
            if time.minutes < 0 {
                time.minutes = 59
                
                time.hours -= 1
                
                if time.hours < 1 {
                    time.hours = 23
                }
            }
            
            currentDay = changeTime(time, toDate: currentDay)
        }
        
        timeLabel.text = currentDay.formattedHoursMinutesLocale
    }
    
    func caloriesNumberToString(_ calories: Double) -> String {
        if (calories - floor(calories)) > 0.001 {
           return String(calories)
        } else {
            return String(Int(calories))
        }
    }
    
    func deleteEmptyRowsFromMealTextView(_ mealTextView: UITextView) -> String {
        let separatedString = addMealTextView.text.components(separatedBy: "\n")
        var cutArray = [String]()

        let letterCharacterSet = CharacterSet.letters
        
        for i in 0..<separatedString.count {
            if (separatedString[i].rangeOfCharacter(from: letterCharacterSet) != nil) {
                 cutArray.append(separatedString[i])
            }
        }
        
        let stringRepresentation = cutArray.joined(separator: "\n")
        
        return stringRepresentation
    }
    
    func dateWithSettingTimeFromDate(_ date: Date, toDate secondDate: Date) -> Date {
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.hour, .minute, .day, .month, .year], from: secondDate)
        components.hour = Int(date.formattedHours)!
        components.minute = Int(date.formattedMinutes)!

        return calendar.date(from: components)!
    }
    
    func changeTime(_ newTime: Time, toDate date: Date) -> Date {
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.hour, .minute, .day, .month, .year], from: date)
        components.hour = newTime.hours
        components.minute = newTime.minutes
        
        return calendar.date(from: components)!
    }
    
    func calculatorButtonPressed(_ index: Int) {
        func getLastElement() -> String {
            let separators = CharacterSet(charactersIn: "+*")
            let separatedString = calculatedString.components(separatedBy: separators)
            let lastElement = separatedString[separatedString.endIndex-1]
           
            return lastElement
        }
        
        switch index {
        case 0...9 :
            //digit button pressed
            if calculatedString == "0" {
                calculatedString = String(index)
            } else {
                let lastElement = getLastElement()
                
                if lastElement.contains(".") {
                    //split lastElement such as 0.23 on two parts 0 and 23
                    let element = lastElement.components(separatedBy: ".")
                    let rightPart = element[element.endIndex-1]
                    
                    if rightPart.characters.count<2 {
                        calculatedString += String(index)
                    }
                    
                } else if !lastElement.hasPrefix("0") {
                    calculatedString += String(index)
                }
            }
            //dot button pressed
        case 10:
            if checkConditions() && !getLastElement().contains(".") {
                calculatedString += "."
            }
            //clean button pressed
        case 11:
            calculatedString = "0"
            calculatedCalories = 0
        case 12:
            //plus button pressed
            if checkConditions() {
                calculatedString += "+"
            }
        case 13:
            //multiply button pressed
            if checkConditions() {
                calculatedString += "*"
            }
            //equal button pressed
        case 14: calculateString()
        case 15:
            if calculatedString != "0" && calculatedString.characters.count > 0 {
                calculatedString = calculatedString.substring(to: calculatedString.characters.index(before: calculatedString.endIndex))
                if calculatedString.characters.count == 0 {
                    calculatedString = "0"
                    calculatedCalories = 0
                }
            }
        default: break
        }
        
        caloriesLabel.text = calculatedString
    }
    
    func checkConditions() -> Bool {
        return (calculatedString != "0"
            && !calculatedString.hasSuffix("*")
            && !calculatedString.hasSuffix("+")
            && !calculatedString.hasSuffix(".")) ? true : false
    }
    
    func calculateString() {
        if checkConditions() {
            let expression = NSExpression(format: calculatedString)
            let result = expression.expressionValue(with: nil, context: nil) as! NSNumber
            
            //check if result have more then two decimal
            let separateResult = String(describing: result).components(separatedBy: ".")
            
            if separateResult.count == 2 && separateResult[separateResult.endIndex-1].characters.count > 2 {
                calculatedString = String(Double(result))
            } else {
                calculatedString = String(describing: result)
            }
            
            calculatedCalories = Double(calculatedString)!
            caloriesLabel.text = calculatedString
        }
    }
    
    func showAddNewMealTextViewKeyboard () {
        UIView.setAnimationsEnabled(false)
        self.addMealTextView.becomeFirstResponder()
        UIView.setAnimationsEnabled(true)
    }
    
    func hideAddNewMealTextViewKeyboard () {
        UIView.setAnimationsEnabled(false)
        self.addMealTextView.resignFirstResponder()
        UIView.setAnimationsEnabled(true)
    }
    
    func createKeyboardScreenshot () -> UIImage {
        let imageSize = UIScreen.main.bounds.size
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        let window = UIApplication.shared.windows.last

        context?.saveGState()
        if window!.responds(to: #selector(UIView.drawHierarchy(in:afterScreenUpdates:))) {
            window!.drawHierarchy(in: window!.bounds, afterScreenUpdates: false)
        } else if let context = context {
            window!.layer.render(in: context)
        }
        context?.restoreGState()

       let image = UIGraphicsGetImageFromCurrentImageContext()
       
       UIGraphicsEndImageContext()

       var rect = addMealKeyboardImageView.frame
       let y = (UIScreen.main.bounds.size.height - keyboardHightConstraint.constant)
       rect = CGRect(x: rect.origin.x * (image?.scale)!,
           y: y  * (image?.scale)!,
           width: rect.size.width * (image?.scale)!,
           height: rect.size.height * (image?.scale)!)

       let imageRef = image?.cgImage?.cropping(to: rect)
       let result = UIImage(cgImage: imageRef!)

       return result
    }
    
    func initViews () {
        addMealTextView.autocorrectionType = UITextAutocorrectionType.no
        addMealTextView.delegate = self
        
        addMealTextView.textColor = AppColor.greyColor()
        addMealTextView.autocapitalizationType = UITextAutocapitalizationType.words
        addMealTextView.font = UIFont().helveticaNeueLTStdWithSize(15)
        
        showAddNewMealTextViewKeyboard()
        
        caloriesLabel.adjustsFontSizeToFitWidth = true
        caloriesLabel.minimumScaleFactor = 0.8
        
        addCaloriesView.isHidden = true
        addTimeView.isHidden = true
        
        segmentedControlView.delegate = self
        calculatorView.delegate = self
        
        datePicker.backgroundColor = AppColor.greenColor()
        datePicker.addTarget(self, action: #selector(NewMealViewController.updateTime), for: UIControlEvents.valueChanged)
        datePicker.locale = Locale.current
    }
    
    func setDefaultValueForViews() {
        if isEdit {
            let meal = mealToEdit!
            
            addMealTextView.text = meal.name
            
            calculatedCalories = meal.calories
            
            currentDay = meal.date as Date
        } else {
            calculatedCalories = 0.0
            
            currentDay = dateWithSettingTimeFromDate(Date(), toDate: currentDay)
        }
        
        let timeString = currentDay.formattedHoursMinutesLocale
        segmentedControlView.timeString = timeString
        timeLabel.text = timeString
        
        datePicker.setDate(currentDay, animated: true)
        
        calculatedString = caloriesNumberToString(calculatedCalories)
        
        caloriesLabel.text = caloriesNumberToString(calculatedCalories)
        segmentedControlView.calories = calculatedCalories
    }
    
    func goFromMealToCalories () {
        addMealKeyboardImageView.image = createKeyboardScreenshot()
        
        addCaloriesLeadingConstraint.constant = view.frame.size.width
        self.addCaloriesView.layoutIfNeeded()
        
        addCaloriesView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addNewMealLeadingConstraint.constant = -self.view.frame.size.width
            self.addCaloriesLeadingConstraint.constant = 0
            
            self.addMealView.layoutIfNeeded()
            self.addCaloriesView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addMealView.isHidden = true
                self.segmentedControlView.lockButtons = false
        })
    }
    
    func goFromMealToTime () {
        addMealKeyboardImageView.image = createKeyboardScreenshot()
        
        addTimeLeadingConstraint.constant = view.frame.size.width
        self.addTimeView.layoutIfNeeded()
        
        addTimeView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addNewMealLeadingConstraint.constant = -self.view.frame.size.width
            self.addTimeLeadingConstraint.constant = 0
            
            self.addMealView.layoutIfNeeded()
            self.addTimeView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addMealView.isHidden = true
                self.segmentedControlView.lockButtons = false
        })
    }
    
    func goFromCaloriesToMeal () {
        addNewMealLeadingConstraint.constant = -view.frame.size.width
        self.addMealView.layoutIfNeeded()
        
        addMealView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addCaloriesLeadingConstraint.constant = self.view.frame.size.width
            self.addNewMealLeadingConstraint.constant = 0
            
            self.addMealView.layoutIfNeeded()
            self.addCaloriesView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addCaloriesView.isHidden = true
                self.segmentedControlView.lockButtons = false
                
                self.addMealKeyboardImageView.image = nil
                self.showAddNewMealTextViewKeyboard()
        })
    }
    
    func goFromCaloriesToTime () {
        addTimeLeadingConstraint.constant = view.frame.size.width
        self.addTimeView.layoutIfNeeded()
        
        addTimeView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addCaloriesLeadingConstraint.constant = -self.view.frame.size.width
            self.addTimeLeadingConstraint.constant = 0
            
            self.addTimeView.layoutIfNeeded()
            self.addCaloriesView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addCaloriesView.isHidden = true
                self.segmentedControlView.lockButtons = false
        })
    }
    
    func goFromTimeToMeal () {
        addNewMealLeadingConstraint.constant = -view.frame.size.width
        self.addMealView.layoutIfNeeded()
        
        addMealView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addTimeLeadingConstraint.constant = self.view.frame.size.width
            self.addNewMealLeadingConstraint.constant = 0
            
            self.addMealView.layoutIfNeeded()
            self.addTimeView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addTimeView.isHidden = true
                self.segmentedControlView.lockButtons = false
                self.addMealKeyboardImageView.image = nil
                self.showAddNewMealTextViewKeyboard()
        })
    }
    
    func goFromTimeToCalories () {
        addCaloriesLeadingConstraint.constant = -view.frame.size.width
        self.addCaloriesView.layoutIfNeeded()
        
        addCaloriesView.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.addTimeLeadingConstraint.constant = self.view.frame.size.width
            self.addCaloriesLeadingConstraint.constant = 0
            
            self.addTimeView.layoutIfNeeded()
            self.addCaloriesView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                self.addTimeView.isHidden = true
                self.segmentedControlView.lockButtons = false
        })
    }
}
