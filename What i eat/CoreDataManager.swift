//
//  CoreDateManager.swift
//  What i eat
//
//  Created by Alex on 2/8/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {

//MARK: Internal Properties
    
    static let sharedInstance = CoreDataManager()
    
//MARK: Private API
    
    fileprivate let appDelegate = UIApplication.shared.delegate as! AppDelegate

//MARK: Internal API
    
    func getExistingMealDates () -> [Date] {
        var dates = [Date]()
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DBMeal")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            let meals = results as! [DBMeal]
            
            for i in 0..<meals.count {
                dates.append(meals[i].date as Date)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

        return dates
    }
    
    func fetchFromCoreData(forDate date: Date) -> [DBMeal] {
        var meals = [DBMeal]()
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DBMeal")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            
            meals = results as! [DBMeal]
           
            var sameDays = [DBMeal]()
            
            for i in 0..<meals.count {
                if compareDays(meals[i].date as Date, withSecondDate: date) {
                    sameDays.append(meals[i])
                }
            }
            
            meals = sameDays
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return meals
    }
    
    func saveToCoreData(_ meal: DBMeal, toArray array: [DBMeal]) -> [DBMeal] {
        var array = array
        let managedContext = appDelegate.managedObjectContext
        
        let entity =  NSEntityDescription.entity(forEntityName: "DBMeal", in:managedContext)
        
        let mealCoreData = DBMeal(entity: entity!, insertInto: managedContext)
        
        mealCoreData.name = meal.name
        mealCoreData.date = meal.date
        mealCoreData.calories = meal.calories
        
        do {
            try managedContext.save()
            array.append(mealCoreData)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        return array
    }
    
    func removeObjectFromCoreData(_ arrayToDeleteObject: [DBMeal], withIndex index: Int) -> [DBMeal] {
        var arrayToDeleteObject = arrayToDeleteObject
        let managedContext = appDelegate.managedObjectContext
        
        managedContext.delete(arrayToDeleteObject[index] as DBMeal)
        
        do {
            try managedContext.save()
            arrayToDeleteObject.remove(at: index)
        } catch let error as NSError  {
            print("Could not delete \(error), \(error.userInfo)")
        }
        
        return arrayToDeleteObject
    }
    
    func updateObjectInCoreData(_ newMeal: DBMeal, arrayToUpdate: [DBMeal], index: Int) -> [DBMeal] {
        let meal = arrayToUpdate[index]
        meal.name = newMeal.name
        meal.calories = newMeal.calories
        meal.date = newMeal.date
        
        do {
            try meal.managedObjectContext?.save()
        } catch let error as NSError  {
            print("Could not update \(error), \(error.userInfo)")
        }
        
        return arrayToUpdate
    }
    
    func compareDays(_ firstDate: Date, withSecondDate secondDate: Date) -> Bool {
        let calendar = Calendar.current
        let comps1 = (calendar as NSCalendar).components([.month , .year , .day], from:firstDate)
        let comps2 = (calendar as NSCalendar).components([.month , .year , .day], from:secondDate)
        
        return (comps1.day == comps2.day) && (comps1.month == comps2.month) && (comps1.year == comps2.year)
    }
}
