//
//  ViewController.swift
//  What i eat
//
//  Created by Alex on 1/18/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import Social
import MessageUI
import CoreData

class MealListViewController: UIViewController {
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var settingsButton: UIButton!
    @IBOutlet fileprivate weak var addMealButton: UIButton!
    @IBOutlet fileprivate weak var shareButton: UIButton!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    @IBOutlet fileprivate weak var dateCaloriesSuffixLabel: UILabel!
    @IBOutlet fileprivate weak var dateCaloriesLabel: UILabel!
    @IBOutlet fileprivate weak var mealTableView: UITableView!
    @IBOutlet fileprivate weak var noMealsView: UIView!
    @IBOutlet fileprivate weak var shelfImageView: UIImageView!
    @IBOutlet fileprivate weak var moveViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var showCalendarButton: UIButton!
    
    @IBOutlet fileprivate weak var changeDateView: UIView!
    @IBOutlet fileprivate weak var todayButton: UIView!
    @IBOutlet weak var calendarView: UIView!

    fileprivate let defaults = Foundation.UserDefaults.standard
    fileprivate var showCalories = true
    fileprivate var selectedDay = Date()
    fileprivate var indexToUpdate = 0
    fileprivate var meals = [DBMeal]()
    
//MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        mealTableView.estimatedRowHeight = 30
        mealTableView.rowHeight = UITableViewAutomaticDimension
        mealTableView.register(UINib(nibName: "MealListTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    
        if selectedDay.dateFormat == TimeStyle.format24 {
               mealTableView.separatorInset.left = 57
        } else {
               mealTableView.separatorInset.left = 76
        }
        
        dateCaloriesLabel.adjustsFontSizeToFitWidth = true
        dateCaloriesLabel.minimumScaleFactor = 0.6
        
        meals = CoreDataManager.sharedInstance.fetchFromCoreData(forDate: selectedDay)
        meals = sortMealsByDate(meals)
        
        changeDateView.isHidden = true
        
        resizeView()
        updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - UItableViewDelegate

extension MealListViewController: UITableViewDelegate {
}

// MARK: - UITableViewDataSource

extension MealListViewController: UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        showNoMealsViewIfNeeded()
    
        return meals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MealTableViewCell
    
        let currentMeal = meals[indexPath.row]
        
        cell.timeValue = currentMeal.formattedTimeString()
        cell.mealName = currentMeal.formattedNameString()
        cell.caloriesValue = showCalories ? currentMeal.formattedCaloriesString() : nil

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            meals = CoreDataManager.sharedInstance.removeObjectFromCoreData(meals, withIndex: indexPath.row)
          
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            
            updateView()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modalNewMealController = NewMealViewController(nibName: "NewMealViewController", bundle: nil)
        modalNewMealController.currentDay = selectedDay
        modalNewMealController.mealToEdit = meals[indexPath.row]
        modalNewMealController.delegate = self
      
        indexToUpdate = indexPath.row
        
        present(modalNewMealController, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

// MARK: - SettingsViewControllerDelegate

extension MealListViewController: SettingsViewControllerDelegate {
    func settingsViewControllerDidChangeSettings(_ control: SettingsViewController) {
        resizeView()
        updateView()
        
        mealTableView.reloadData()
    }
}

// MARK: - NewFoodViewControllerDelegate

extension MealListViewController: NewMealViewControllerDelegate {
    func newMealViewController(_ controller: NewMealViewController, didAddMeal meal: DBMeal) {
        meals = CoreDataManager.sharedInstance.saveToCoreData(meal, toArray: meals)
        meals = sortMealsByDate(meals)
        
        mealTableView.reloadData()
        
        updateView()
    }
    
    func newMealViewController(_ controller: NewMealViewController, didUpdateMeal meal: DBMeal) {
        meals = CoreDataManager.sharedInstance.updateObjectInCoreData(meal, arrayToUpdate: meals, index: indexToUpdate)
        meals = sortMealsByDate(meals)
        
        mealTableView.reloadData()
        
        updateView()
    }
}

// MARK: - MFMailComposeViewControllerDelegate, Share

extension MealListViewController: MFMailComposeViewControllerDelegate {
    @IBAction func share(_ sender: UIButton) {
        let shareAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
       
        let eMail = UIAlertAction(title: "E-Mail", style: .default, handler: { (action) -> Void in
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setSubject("")
                mail.setMessageBody("", isHTML: true)
                self.present(mail, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Error", message: "Error to send email.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        
        let facebook = UIAlertAction(title: "Facebook", style: .default, handler: { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                facebookSheet.setInitialText("Share on Facebook")
                self.present(facebookSheet, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
       
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel Button Pressed")
        })

        shareAlertController.addAction(eMail)
        shareAlertController.addAction(facebook)
        shareAlertController.addAction(cancel)
        
        present(shareAlertController, animated: true, completion: nil)
    }
    
    //MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

//MARK: RSDFDatePickerViewDelegate

extension MealListViewController: RSDFDatePickerViewDelegate {
    func datePickerView(_ view: RSDFDatePickerView!, shouldSelect date: Date!) -> Bool {
        selectedDay = date
        
        changeDateView.isHidden = true
        
        meals = CoreDataManager.sharedInstance.fetchFromCoreData(forDate: selectedDay)
        meals = sortMealsByDate(meals)
        
        mealTableView.reloadData()
       
        updateView()
        
        return true
    }
}

//MARK: RSDFDatePickerViewDataSource

extension MealListViewController: RSDFDatePickerViewDataSource {
    func datePickerView(_ view: RSDFDatePickerView!, shouldMark date: Date!) -> Bool {
        let existDays = CoreDataManager.sharedInstance.getExistingMealDates()
        
        for i in 0..<existDays.count {
            if CoreDataManager.sharedInstance.compareDays(date, withSecondDate: existDays[i]) {
                return true
            }
        }
        
        return false
    }
}

//MARK: - Private

private extension MealListViewController {
    @IBAction func addMeal(_ sender: UIButton) {
        let modalNewFoodController = NewMealViewController(nibName: "NewMealViewController", bundle: nil)
        modalNewFoodController.currentDay = selectedDay
        modalNewFoodController.delegate = self
        
        present(modalNewFoodController, animated: true, completion: nil)
    }
    
    @IBAction func settings(_ sender: UIButton) {
        let modalSettingsController = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
        modalSettingsController.delegate = self
        
        present(modalSettingsController, animated: true, completion: nil)
    }
    
    @IBAction func showCalendar(_ sender: UIButton) {
        changeDateView.isHidden = false
        
        let calendarPickerView = RSDFDatePickerView(frame: calendarView.frame)
        calendarPickerView.delegate = self
        calendarPickerView.dataSource = self
        calendarPickerView.select(selectedDay)
        calendarPickerView.scroll(to: selectedDay, animated: false)
        
        calendarView.addSubview(calendarPickerView)
    }
    
    @IBAction func showTodayMeals(_ sender: UIButton) {
        changeDateView.isHidden = true
        selectedDay = Date()
      
        meals = CoreDataManager.sharedInstance.fetchFromCoreData(forDate: selectedDay)
        meals = sortMealsByDate(meals)
        
        mealTableView.reloadData()
        
        updateView()
    }
    
    func resizeView() {
        showCalories = defaults.bool(forKey: UserDefaults.caloriesEnabled)
        
        if defaults.object(forKey: UserDefaults.caloriesEnabled) == nil {
            defaults.set(true, forKey: UserDefaults.caloriesEnabled)
            showCalories = true
        }
        
        dateLabel.isHidden = !showCalories
        moveViewConstraint.constant = showCalories ? 115 : 85
    }
    
    func updateView() {
        if showCalories {
            dateCaloriesLabel.text = calculateCaloriesSum()
            dateCaloriesSuffixLabel.text = "kcal"
            
            let monthString = formattedMonth(selectedDay) as String
            let dayString = formattedDay(selectedDay) as String
            
            dateLabel.text = monthString + ", " + dayString
        } else {
            dateCaloriesLabel.text = formattedDay(selectedDay) as String
            dateCaloriesSuffixLabel.text = formattedMonth(selectedDay) as String
        }
    }
    
    func showNoMealsViewIfNeeded() {
        if meals.count == 0 {
            noMealsView.isHidden = false
            mealTableView.isHidden = true
        } else {
            noMealsView.isHidden = true
            mealTableView.isHidden = false
        }
    }

    func sortMealsByDate(_ meals: [DBMeal]) -> [DBMeal] {
        return meals.sorted(by: {$0.date.compare($1.date as Date) == ComparisonResult.orderedAscending})
    }
    
    func calculateCaloriesSum() -> String {
        var caloriesSum = 0.0
     
        for i in 0..<meals.count {
            caloriesSum += meals[i].calories
        }
        
        if (caloriesSum - floor(caloriesSum)) > 0.001 {
            return String(caloriesSum)
        } else {
            return String(Int(caloriesSum))
        }
    }
    
    func formattedDay (_ date: Date) -> NSString {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        
        return formatter.string(from: date) as NSString
    }
    
    func formattedMonth (_ date: Date) -> NSString {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM"
        
        return formatter.string(from: date) as NSString
    }
}
