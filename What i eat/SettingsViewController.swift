//
//  SettingsViewController.swift
//  What i eat
//
//  Created by Alex on 1/19/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

protocol SettingsViewControllerDelegate: class {
    func settingsViewControllerDidChangeSettings(_ control: SettingsViewController)
}

class SettingsViewController: UIViewController {
    
//MARK: Internal Properties
    
    weak var delegate: SettingsViewControllerDelegate? = nil
    
//MARK: Private Properties
    
    @IBOutlet fileprivate weak var cancelButton: UIButton!
    @IBOutlet fileprivate weak var doneButton: UIButton!
    @IBOutlet fileprivate weak var settingsLabel: UILabel!
    @IBOutlet fileprivate weak var settingsTableView: UITableView!
    
    fileprivate let defaults = Foundation.UserDefaults.standard
    fileprivate let settings = ["Calories Tracking", "Fx Sounds"]
    
    fileprivate var caloriesEnabled = true
    fileprivate var fxSoundEnabled = true
    
//MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsTableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        settingsTableView.allowsSelection = false
        
        caloriesEnabled = defaults.bool(forKey: UserDefaults.caloriesEnabled)
        fxSoundEnabled = defaults.bool(forKey: UserDefaults.fxSoundEnabled)
       
        if defaults.object(forKey: UserDefaults.fxSoundEnabled) == nil {
            fxSoundEnabled = true
            defaults.set(true, forKey: UserDefaults.fxSoundEnabled)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - UItableViewDelegate

extension SettingsViewController: UITableViewDelegate {
}

// MARK: - UItableViewDataSource

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingsTableViewCell
        
        cell.stateSwitch.addTarget(self, action: #selector(SettingsViewController.changeState(_:)), for: UIControlEvents.valueChanged)
        cell.stateSwitch.tag = indexPath.row
        cell.propertyString = settings[indexPath.row]
       
        switch indexPath.row {
        case 0:
            cell.propertyState = caloriesEnabled
        case 1:
            cell.propertyState = fxSoundEnabled
        default: break
        }
        
        return cell
    }
    
    func changeState(_ sender: UISwitch) {
        switch sender.tag {
        case 0:
            defaults.set(sender.isOn, forKey: UserDefaults.caloriesEnabled)
        case 1:
            defaults.set(sender.isOn, forKey: UserDefaults.fxSoundEnabled)
        default: break
        }
    }
}

//MARK: Private

private extension SettingsViewController {
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: {})
    }
    
    @IBAction func done(_ sender: UIButton) {
        delegate?.settingsViewControllerDidChangeSettings(self)
        
        dismiss(animated: true, completion: {})
    }
}
