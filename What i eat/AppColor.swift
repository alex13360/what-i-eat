//
//  AppColor.swift
//  What i eat
//
//  Created by Alex on 1/25/16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import UIKit

struct AppColor {
    static func greenColor() -> UIColor{
        return UIColor(hex: "#24BEB2")
    }
   
    static func greyColor() -> UIColor {
        return UIColor(hex: "#454F53")
    }
    
    static func lightGreyColor() -> UIColor {
        return UIColor(hex: "#5b696f")
    }
   
    static func darkGreyColor() -> UIColor {
        return UIColor(hex: "#6F7179")
    }
  
    static func whiteColor() -> UIColor {
        return UIColor(hex: "#FFFFFF")
    }
}